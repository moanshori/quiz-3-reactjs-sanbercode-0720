import React from 'react';
import './App.css';
import Index from './quiz3/index'
import { LoginProvider } from './quiz3/Page/Login/LoginContext';

function App() {
  return (
    <div className="App">
      <LoginProvider>
        <Index />
      </LoginProvider>
    </div>
  );
}

export default App;
