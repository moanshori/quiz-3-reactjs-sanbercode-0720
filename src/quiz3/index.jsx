import React, { useContext } from 'react';
import './index.css';
import logo from './public/image/logo.png'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import About from './Page/About/About';
import Home from './Page/Home/Home'
import MovieManager from './Page/MovieManager/MovieManager';
import Login from './Page/Login/Login'
import { LoginContext } from './Page/Login/LoginContext';

function Index() {
    const [isLogin, setisLogin] = useContext(LoginContext)

    return (
        <>
            <head>
                <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet" />
            </head>
            <body>
                <Router>
                    <div>
                        <header>
                            <img id="logo" src={logo} width="200px" alt="logo" />
                            <nav>
                                <ul>
                                    <li><Link to="/">Home</Link></li>
                                    <li><Link to="/about">About</Link></li>
                                    {isLogin === true && (
                                        <li><Link to="/managemovie">Movie List Editor</Link></li>
                                    )}
                                    <li><Link to="/login">Login</Link></li>
                                </ul>
                            </nav>
                        </header>
                        <section>
                            <Switch>
                                <Route path="/about">
                                    <About />
                                </Route>
                                <Route path="/managemovie">
                                    <MovieManager />
                                </Route>
                                <Route path="/login">
                                    <Login />
                                </Route>
                                <Route path="/">
                                    <Home />
                                </Route>
                            </Switch>
                        </section>
                        <footer>
                            <h5>copyright &copy; 2020 by Sanbercode</h5>
                        </footer>
                    </div>
                </Router>
            </body>
        </>
    )
}

export default Index

