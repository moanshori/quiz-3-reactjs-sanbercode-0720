import React, { useState, useEffect } from 'react';
import axios from 'axios'

const Home = () => {
    const [dataMovie, setDataMovie] = useState([])
    let [data, setData] = useState([])

    useEffect(() => {
        axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then(res => {
                setDataMovie(res.data.map(item => {
                    return {
                        title: item.title,
                        description: item.description,
                        year: item.year,
                        duration: item.duration,
                        genre: item.genre,
                        rating: item.rating
                    }
                }));
            }, [dataMovie]);
        setData(dataMovie.sort(function (a, b) {
            return b.rating - a.rating
        }))
    })

    return (
        <>
            <h1 style={{ textAlign: "center" }}>Daftar Film-Film Terbaik</h1>
            {data.map((val, index) => {
                return (<>
                    <div style={{ padding: "10px", border: "1px solid #ccc" }}>
                        <h2 style={{ textAlign: "left", color: "blue" }}>{val.title}</h2>
                        <hr style={{ borderTop: "1px solid #bbb" }} />
                        <p style={{ textAlign: "left" }}>
                            <b>Rating :  {val.rating}<br />
                            Tahun: {val.year}<br />
                            Durasi : {val.duration} menit<br />
                            Genre: {val.genre}<br />
                                <br />
                            Deskripsi: {val.description}<br />
                            </b>
                        </p>
                    </div>
                    <br />
                </>
                )
            })}
        </>
    )
}

export default Home