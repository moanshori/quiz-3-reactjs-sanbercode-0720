import React, { useContext } from 'react';
import { LoginContext } from './LoginContext'

const Login = () => {
    const [isLogin, setisLogin] = useContext(LoginContext)
    let label = ""

    isLogin ? label = "Log Out" : label = "Login"

    const checkLogin = () => {
        setisLogin(isLogin == false ? true : false)
    }

    return (
        <>
            <h1>LOGIN</h1>
            <div style={{ padding: "10px", border: "1px solid #ccc" }}>
                <p style={{ textAlign: "left", color: "brown" }}>
                    Login untuk mendapatkan akses manage Movie. <br/>
                    Termasuk menambahkan, menghapus, dan merubah data.
                </p>
                <button onClick={checkLogin} style={{
                    padding: "15px 100px", 
                    border: "none", 
                    color: "white", 
                    backgroundColor: "pink"}}>
                    {label}
                </button>
            </div>
        </>
    )
}

export default Login