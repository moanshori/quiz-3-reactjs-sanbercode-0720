import React, { useState, createContext } from "react";

export const LoginContext = createContext();
export const LoginProvider = props => {
    const [isLogin, setisLogin] = useState(false)

    return (
        <LoginContext.Provider value={[isLogin, setisLogin]}>
            {props.children}
        </LoginContext.Provider>
    )
}