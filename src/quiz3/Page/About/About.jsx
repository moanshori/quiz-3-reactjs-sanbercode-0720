import React from 'react';

let dataAbout = [{
    name: "Mochammad Anshori",
    email: "aanshori@surel.com",
    os: "Windows 10",
    gitlab: "@moanshori",
    telegram: "@moanshori"
}]

class ShowData extends React.Component {
    render() {
        return (
            <>
                <ol>
                    <li><strong style={{ width: "100px" }}>Nama: </strong>{this.props.data.name}</li>
                    <li><strong style={{ width: "100px" }}>Email: </strong>{this.props.data.email}</li>
                    <li><strong style={{ width: "100px" }}>Sistem Operasi yang digunakan: </strong>{this.props.data.os}</li>
                    <li><strong style={{ width: "100px" }}>Akun Gitlab: </strong>{this.props.data.gitlab}</li>
                    <li><strong style={{ width: "100px" }}>Akun Telegram: </strong>{this.props.data.telegram}</li>
                </ol>
            </>
        )
    }
}

class About extends React.Component {
    render() {
        return (
            <>
                <div style={{ padding: "10px", border: "1px solid #ccc" }}>
                    <h1 style={{ textAlign: "center" }}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                    {
                        dataAbout.map(item => {
                            return (
                                <ShowData data={item} />
                            )
                        })
                    }
                </div>
            </>
        )
    }
}

export default About