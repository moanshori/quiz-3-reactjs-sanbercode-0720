import React, { useState, useEffect } from 'react';
import axios from 'axios';

const MovieManager = () => {
    const [dataMovie, setDataMovie] = useState([])
    const [inputTitle, setinputTitle] = useState("")
    const [inputYear, setinputYear] = useState("")
    const [inputDuration, setinputDuration] = useState("")
    const [inputGenre, setinputGenre] = useState("")
    const [inputRating, setinputRating] = useState("")
    const [inputDesc, setinputDesc] = useState("")
    const [editCond, seteditCond] = useState("")

    useEffect(() => {
        axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then(res => {
                setDataMovie(res.data.map(item => {
                    return {
                        id: item.id,
                        title: item.title,
                        year: item.year,
                        duration: item.duration,
                        genre: item.genre,
                        rating: item.rating,
                        description: item.description,
                    }
                }));
            }, [dataMovie])
    })

    const handleChangeTitle = (event) => {
        setinputTitle(event.target.value)
    }
    const handleChangeYear = (event) => {
        setinputYear(event.target.value)
    }
    const handleChangeDuration = (event) => {
        setinputDuration(event.target.value)
    }
    const handleChangeGenre = (event) => {
        setinputGenre(event.target.value)
    }
    const handleChangeRating = (event) => {
        setinputRating(event.target.value)
    }
    const handleChangeDesc = (event) => {
        setinputDesc(event.target.value)
    }

    const handleEdit = (event) => {
        console.log("edit " + event.target.value)
        let data = dataMovie.find(x =>
            x.id == event.target.value
        )
        console.log(data)
        setinputTitle(data.title)
        setinputRating(data.rating)
        setinputDuration(data.duration)
        setinputGenre(data.genre)
        setinputYear(data.year)
        setinputDesc(data.description)
        seteditCond(event.target.value)
    }

    const handleDelete = (event) => {
        console.log("delete " + event.target.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${event.target.value}`)
            .then(res => {
                console.log(res);
                console.log(res.data);
            })

    }

    const handleSubmit = (event) => {
        event.preventDefault()
        let data = {
            title: inputTitle,
            description: inputDesc,
            year: inputYear,
            duration: inputDuration,
            genre: inputGenre,
            rating: inputRating
        }

        if (editCond != "") {
            axios.put(`http://backendexample.sanbercloud.com/api/movies/${editCond}`, data)
                .then(res => {
                    console.log("edit")
                    console.log(res.data);
                })
        } else {
            axios.post(`http://backendexample.sanbercloud.com/api/movies`, data)
                .then(res => {
                    console.log("submit")
                    console.log(res.data);
                })
        }
        handleClear()
    }

    const handleClear = (event) => {
        setinputTitle("")
        setinputRating("")
        setinputDuration("")
        setinputGenre("")
        setinputYear("")
        setinputDesc("")
        seteditCond("")
    }

    return (
        <>
            <h1>Manage Movie</h1>
            <h2 style={{textAlign:"left", color:"brown"}}>Data Movie</h2>
            <table style={{ color: "white", display: "table-cell" }}>
                <thead style={{ backgroundColor: "#AAAAAA" }}>
                    <tr>
                        <th>Judul</th>
                        <th>Rating</th>
                        <th>Durasi</th>
                        <th>Genre</th>
                        <th>Tahun</th>
                        <th>Deskripsi</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {dataMovie.map((val, index) => {
                        return (
                            <tr style={{ textAlign: "left", backgroundColor: "white" }}>
                                <td style={{ backgroundColor: "#FF7F50" }}>{val.title}</td>
                                <td style={{ backgroundColor: "#FF7F50" }}>{val.rating}</td>
                                <td style={{ backgroundColor: "#FF7F50" }}>{val.duration}</td>
                                <td style={{ backgroundColor: "#FF7F50" }}>{val.genre}</td>
                                <td style={{ backgroundColor: "#FF7F50" }}>{val.year}</td>
                                <td style={{ backgroundColor: "#FF7F50" }}>{val.description}</td>
                                <td>
                                    <button onClick={handleEdit} value={val.id} style={{width: "100%"}}>Edit</button>
                                    <button onClick={handleDelete} value={val.id} style={{width: "100%"}}>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <h2 style={{textAlign:"left", color:"brown"}}>Form</h2>
            <label>{editCond}</label>
            <form onSubmit={handleSubmit}>
                <table style={{ textAlign: "left", margin: "auto" }}>
                    <tr>
                        <td>Judul :</td>
                        <td><input type="text" value={inputTitle} onChange={handleChangeTitle} style={{ width: "500px" }} /></td>
                    </tr>
                    <tr>
                        <td>Rating :</td>
                        <td>
                            <input type="radio" id="1" name="rating" value="1" onChange={handleChangeRating} checked={inputRating == 1} />
                            <label for="1" style={{ paddingRight: "12px" }}>1</label>
                            <input type="radio" id="2" name="rating" value="2" onChange={handleChangeRating} checked={inputRating == 2} />
                            <label for="2" style={{ paddingRight: "12px" }}>2</label>
                            <input type="radio" id="3" name="rating" value="3" onChange={handleChangeRating} checked={inputRating == 3} />
                            <label for="3" style={{ paddingRight: "12px" }}>3</label>
                            <input type="radio" id="4" name="rating" value="4" onChange={handleChangeRating} checked={inputRating == 4} />
                            <label for="4" style={{ paddingRight: "12px" }}>4</label>
                            <input type="radio" id="5" name="rating" value="5" onChange={handleChangeRating} checked={inputRating == 5} />
                            <label for="5" style={{ paddingRight: "12px" }}>5</label>
                            <input type="radio" id="6" name="rating" value="6" onChange={handleChangeRating} checked={inputRating == 6} />
                            <label for="6" style={{ paddingRight: "12px" }}>6</label>
                            <input type="radio" id="7" name="rating" value="7" onChange={handleChangeRating} checked={inputRating == 7} />
                            <label for="7" style={{ paddingRight: "12px" }}>7</label>
                            <input type="radio" id="8" name="rating" value="8" onChange={handleChangeRating} checked={inputRating == 8} />
                            <label for="8" style={{ paddingRight: "12px" }}>8</label>
                            <input type="radio" id="9" name="rating" value="9" onChange={handleChangeRating} checked={inputRating == 9} />
                            <label for="9" style={{ paddingRight: "12px" }}>9</label>
                            <input type="radio" id="10" name="rating" value="10" onChange={handleChangeRating} checked={inputRating == 10} />
                            <label for="10">10</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Durasi :</td>
                        <td><input type="number" value={inputDuration} onChange={handleChangeDuration} style={{ width: "500px" }} /></td>
                    </tr>
                    <tr>
                        <td>Genre :</td>
                        <td><input type="text" value={inputGenre} onChange={handleChangeGenre} style={{ width: "500px" }} /></td>
                    </tr>
                    <tr>
                        <td>Tahun :</td>
                        <td><input type="number" value={inputYear} onChange={handleChangeYear} style={{ width: "500px" }} /></td>
                    </tr>
                    <tr>
                        <td>Deskripsi :</td>
                        <td><textarea rows="4" value={inputDesc} onChange={handleChangeDesc} style={{ width: "500px" }} /></td>
                    </tr>
                    <tr>
                        <td colspan="2" style={{ textAlign: "center" }}>
                            <button style={{ width: "100%", paddingTop: "5px", paddingBottom: "5px", backgroundColor: "#FF401A", color: "white", border: "none" }}>Submit</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style={{ textAlign: "center" }}>
                            <button onClick={handleClear} style={{ width: "100%", paddingTop: "5px", paddingBottom: "5px", backgroundColor: "#FF401A", color: "white", border: "none" }}>Clear</button>
                        </td>
                    </tr>
                </table>
            </form>
        </>
    )
}

export default MovieManager